from setuptools import setup
setup(name='test_package', version='0.5', description='Testing installation of Package',
      url='#', author='briandra3', author_email='test@test.com', license='MIT',
      packages=['test_package', 'test_package.test_folder'], zip_safe=False)
